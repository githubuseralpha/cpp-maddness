#include "Matrix.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <vector>


Matrix::Matrix(int m, int n) :m(m), n(n), values(new float[n * m]) {
	fill(0);
}

Matrix::Matrix(const Matrix& other) : m(other.m), n(other.n), values(new float [other.m * other.n]) {
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++) {
			values[i * n + j] = other.values[i* n + j];
		}
	}
}

Matrix& Matrix::operator=(const Matrix& other)
{
	if (this == &other)
		return *this;

	float* new_values = new float [other.m * other.n];

	std::memcpy(new_values, other.values, other.n  * other.m * sizeof(float));
	
	delete[] values;

	m = other.m;
	n = other.n;
	values = new_values;

	return *this;
}


Matrix::~Matrix() 
{
	delete[] values;
}

void Matrix::fill(float val) 
{
	for (int i = 0; i < m; i++) 
	{
		for (int j = 0; j < n; j++)
		{
			values[i * n + j] = val;
		}
	}
}

void Matrix::fill_random(float min_val, float max_val)
{
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			values[i * n + j] = min_val + static_cast <float> (rand()) / 
				static_cast <float> (RAND_MAX/(max_val - min_val));
		}
	}
}

void Matrix::print() const
{
	for (int i = 0; i < m; i++) 
	{
		if (i == PRINT_LIMIT && m - i > PRINT_LIMIT)
		{
			std::cout << "...\n";
			continue;
		}
		else if (i >= PRINT_LIMIT && m - i > PRINT_LIMIT)
			continue;
		std::cout << "[ ";
		for (int j = 0; j < n; j++) 
		{
			if (j == PRINT_LIMIT && n - j > PRINT_LIMIT) 
			{
				std::cout << " ... ";
				continue;
			}
			else if (j >= PRINT_LIMIT && n - j > PRINT_LIMIT)
				continue;

			std::stringstream stream;
			stream << std::fixed << std::setprecision(3) << values[i * n + j];
			std::cout << stream.str() << " ";
		}
		std::cout << " ]\n";
	}
}

Matrix Matrix::transpose() const {
	Matrix new_mat(n, m);
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			new_mat.values[j * m + i] = values[i * n + j];
		}
	}
	return new_mat;
}

Matrix Matrix::mul(const Matrix& other) {
	Matrix res(m, other.n);
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			for (int k = 0; k < other.n; k++) {
				res.values[i * res.n + k] += values[i * n + j] * other.values[j * other.n + k];
			}
		}
	}
	return res;
}

Vector::Vector(const Matrix& mat, int row, int start, int end) :
	values(new float[end - start]), n(end - start)
{
	std::memcpy(values, mat.values + row * mat.n + start, n * sizeof(float));
}

void Vector::print() const {
	std::cout << "[ ";
	for (int i = 0; i < n; i++) {
		std::cout << values[i] << " ";
	}
	std::cout << " ]\n";
}

Vector::Vector(const Vector& other)// : values(new float[other.n]), n(other.n)
{
	values = new float[other.n];
	n = other.n;
	std::memcpy(values, other.values, n * sizeof(float));
}

Vector& Vector::operator=(const Vector& other) 
{
	if (this == &other)
		return *this;

	float* new_values = new float[other.n];
	std::memcpy(new_values, other.values, other.n * sizeof(float));

	delete[] values;

	n = other.n;
	values = new_values;

	return *this;
}


float Vector::distance(const Vector& other) const
{
	std::vector<float> distances(n);
	std::transform(values, values + n, other.values, distances.begin(), 
		[](float a, float b) -> float { return (a - b) * (a - b);  });
	float distance_squared = std::accumulate(distances.begin(), distances.end(), 0.0f);
	return sqrt(distance_squared);
}

Vector Vector::operator+(const Vector& other) const {
	Vector new_vec(n);
	std::transform(values, values + n, other.values, new_vec.values, std::plus<>());
	return new_vec;
}

Vector Vector::operator*(float other) const {
	Vector new_vec(n);
	//std::transform(values, values + n, new_vec.values,
	//	[other](float val) -> float { return val * other; });
	for (int i = 0; i < n; i++)
		new_vec.values[i] = values[i] * other;
	return new_vec;
}

/*
Vector& Vector::operator+=(const Vector& other) {
	std::transform(values, values + n, other.values, values, std::plus<>());
}
*/

bool Vector::operator==(const Vector& other) const {
	float e = 0.001;
	bool equals = std::transform_reduce(values, values + n, other.values, true, std::multiplies<>(),
		[e](float a, float b) -> bool {
		return abs(a - b) < e;
	});
	return equals;
}

void Vector::fill(float val) {
	std::fill(values, values + n, val);
}


Vector::Vector(int n) : values(new float[n]), n(n) {
	fill(0);
}

float Vector::dot(const Vector& other) const {
	float res = 0;
	for (int i = 0; i < n; i++)
		res += values[i] * other.values[i];
	return res;
}
