#include "Matrix.h"
#include "Maddness.h"
#include "Utils.h"
#include "Timer.h"
#include <vector>
#include <iostream>
#include <immintrin.h>
#include <array>
using namespace std;

int main() {

	Matrix A(1024, 512);
	A.fill_random(0, 100);

	Matrix B(512, 1024);
	B.fill_random(0, 50);

	Maddness madd(25, 16);
	madd.fit(A, B);
	{
		std::cout << "MADDNESS MULTIPLICATION\n";
		Timer t;
		madd.predict(A);
	}
	{
		std::cout << "\nCLASSIC MULTIPLICATION\n";
		Timer t;
		A.mul(B);
	}
}
