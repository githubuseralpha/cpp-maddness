#include "Hash.h"
#include <algorithm>
#include <numeric>
#include <iostream>
#include <immintrin.h>

void HashTree::quantize(
	const Matrix& matrix,
	uint8_t** columns,
	int col
) {
	constexpr int num_floats = 8;
	float buf[num_floats];
	__m256 sub = _mm256_set1_ps(offset);
	__m256 mul = _mm256_set1_ps(inverse_scaling);

	for (int8_t t = 0; t < TREE_DEPTH; t++) {
		for (int row = 0; row < matrix.m; row += num_floats) {
			__m256i indexes = _mm256_setr_epi32(
				row * matrix.n + col + indices[t],
				(row + 1) * matrix.n + col + indices[t],
				(row + 2) * matrix.n + col + indices[t],
				(row + 3) * matrix.n + col + indices[t],
				(row + 4) * matrix.n + col + indices[t],
				(row + 5) * matrix.n + col + indices[t],
				(row + 6) * matrix.n + col + indices[t],
				(row + 7) * matrix.n + col + indices[t]
			);
			__m256 src = _mm256_i32gather_ps(matrix.values, indexes, sizeof(float));
			__m256 subtracted = _mm256_sub_ps(src, sub);
			__m256 multiplied = _mm256_mul_ps(subtracted, mul);
			//__m256i quantized = _mm256_castps_si256(multiplied);
			//_mm256_store_si256((__m256i*) (columns[t] + row), quantized);
			_mm256_store_ps(buf, multiplied);
			for (int i = 0; i < num_floats; i++)
				columns[t][row + i] = buf[i];
		}
	}
}


__m256i HashTree::hash_columns(uint8_t** columns, int row) {
	const int num_ints = 32;
	__m256i i = _mm256_set1_epi8(0);
	__m256i simd_thresholds = _mm256_load_si256((__m256i*)thresholds);
	__m256i scale = _mm256_set1_epi8(2);
	__m256i incr = _mm256_set1_epi8(1);
		 
	for (int8_t t = 0; t < TREE_DEPTH; t++) {
		__m256i thresholds = _mm256_shuffle_epi8(simd_thresholds, i);
		__m256i values = _mm256_load_si256((__m256i*)(columns[t] + row));
		__m256i b = _mm256_cmpgt_epi8(values, thresholds);
		i = _mm256_mul_epi32(i, scale);
		i = _mm256_add_epi32(i, incr);
		i = _mm256_sub_epi32(i, b);
	}
	return i;
}

auto HashTree::apply_split(float threshold, int j, const std::vector<Vector>& bucket) {
	std::vector<Vector> bucket1;
	std::vector<Vector> bucket2;

	for (const Vector& v : bucket) {
		if (v.values[j] < threshold)
			bucket1.push_back(v);
		else
			bucket2.push_back(v);
	}
	return std::make_pair(bucket1, bucket2);
}

auto HashTree::get_params_for_level(
	std::vector<std::vector<Vector>>& buckets, int vector_size
) {
	std::vector<int> indices = select_indices(vector_size);
	int b = buckets.size();
	float min_loss = std::numeric_limits<float>::max();
	int min_j = -1;
	std::vector<float> min_thresholds;

	for (int j : indices) {
		float loss = 0;
		std::vector<float> thresholds(b);
		for (int i = 0; i < b; i++) {
			if (buckets[i].size() <= 1) {
				thresholds[i] = 0;
				continue;
			}

			auto [threshold, curr_loss] = split_threshold(j, buckets[i]);
			loss += curr_loss;
			thresholds[i] = threshold;
		}
		if (loss < min_loss) {
			min_loss = loss;
			min_j = j;
			min_thresholds = thresholds;
		}
	}

	std::vector<std::vector<Vector>> new_buckets;
	for (int i = 0; i < b; i++) {
		auto [bucket1, bucket2] = apply_split(min_thresholds[i], min_j, buckets[i]);
		new_buckets.push_back(bucket1);
		new_buckets.push_back(bucket2);
	}
	return std::make_tuple(new_buckets , min_j, min_thresholds);
}

std::vector<int> HashTree::select_indices(int range) {
	std::vector<int> indices(range);
	std::iota(indices.begin(), indices.end(), 0);
	return indices;
}

std::pair<float, float> HashTree::split_threshold(int j, std::vector<Vector>& bucket) {
	std::sort(bucket.begin(), bucket.end(), [j](const Vector& a, const Vector& b) -> bool {
		return a.values[j] < b.values[j];
	});
	std::vector<float> ssed_head = cumulative_sse(bucket, false);
	std::vector<float> ssed_tail = cumulative_sse(bucket, true);
	std::vector<float> losses(ssed_head.size() - 1);

	/*
	std::cout << "\n\n";
	for (auto elem : ssed_head)
		std::cout << elem << ", ";
	std::cout << "\n\n";
	for (auto elem : ssed_tail)
		std::cout << elem << ", ";
	std::cout << "\n\n";
	*/
	for (int n = 0; n < losses.size(); n++)
		losses[n] = ssed_head[n] + ssed_tail[losses.size() - n];


	auto min_it = std::min_element(losses.begin(), losses.end());
	int argmin = min_it - losses.begin();
	float loss = *min_it;

	float split_val = (bucket[argmin].values[j] + bucket[argmin + 1].values[j]) / 2;
	return { split_val, loss };
}

std::vector<float> HashTree::cumulative_sse(std::vector<Vector> bucket, bool reverse) {
	int N = bucket.size();
	int D = bucket[0].n;
	if (reverse) 
		std::reverse(bucket.begin(), bucket.end());
	
	std::vector<float> out(N);
	std::vector<float> cumx(D);
	std::vector<float> cumx2(D);

	out[0] = 0;
	for (int d = 0; d < D; d++) {
		cumx[d] = bucket[0].values[d];
		cumx2[d] = bucket[0].values[d] * bucket[0].values[d];
	}
	for (int n = 1; n < N; n++) {
		out[n] = 0;
		for (int d = 0; d < D; d++) {
			cumx[d] += bucket[n].values[d]; // maybe n instead of 0??
			cumx2[d] += bucket[n].values[d] * bucket[n].values[d]; // same 
			out[n] += cumx2[d] - (cumx[d] * cumx[d] / (n + 1));
		}
	}
	return out;
}

std::vector<std::vector<Vector>> HashTree::learn_parameters(std::vector<std::vector<Vector>>& buckets) {
	int thresh_pos = 0;
	int vector_size = buckets[0][0].n;
	constexpr int thresholds_size = ipow(2, TREE_DEPTH) - 1;
	float temp_thresholds[thresholds_size];
	float max_val = std::numeric_limits<float>::min();
	offset = std::numeric_limits<float>::max();

	for (int t = 0; t < TREE_DEPTH; t++) {
		auto [new_buckets, min_j, min_thresholds] = get_params_for_level(buckets, vector_size);
		buckets = new_buckets;
		indices[t] = min_j;
		for (int i = 0; i < min_thresholds.size(); i++, thresh_pos++) {
			temp_thresholds[thresh_pos] = min_thresholds[i];
			offset = std::min(offset, min_thresholds[i]);
			max_val = std::max(max_val, min_thresholds[i]);
		}
	}
	inverse_scaling = 255.0 / (max_val - offset);
	for (int i = 0; i < thresholds_size; i++) {
		thresholds[i] = (temp_thresholds[i] - offset) * inverse_scaling;
	}
	return buckets;
}


