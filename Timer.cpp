#include "Timer.h";
#include <iostream>
#include <numeric>


Timer::Timer() {
	start = std::chrono::high_resolution_clock::now();
}

Timer::~Timer() {
	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<float> duration = end - start;
	std::cout << "Time: " << duration.count() * 1000.0f << "ms.\n";
}
