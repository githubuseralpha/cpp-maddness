#pragma once
#include "Matrix.h"
#include <vector>

std::vector<int> generate_random_numbers(int start, int end, int n);
std::vector<Vector> k_means(const std::vector<Vector>& vectors, int k);

constexpr int ipow(int num, unsigned int pow)
{
	return (pow >= sizeof(unsigned int) * 8) ? 0 :
		pow == 0 ? 1 : num * ipow(num, pow - 1);
}
