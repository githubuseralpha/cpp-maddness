#pragma once

#include "Matrix.h"
#include "Utils.h"
#include "Hash.h"
#include <vector>

struct Maddness {
	int n, c, k;
	float offset, inverse_scaling;
	uint8_t*** lookup_table;
	std::vector<std::vector<Vector>> prototypes;
	std::vector<HashTree> hash_prototypes;
	std::vector<std::vector<Vector>> averages;
	std::vector<int> indices;

	Maddness(int c, int k);
	~Maddness();

	Matrix predict(const Matrix& matrix);
	void learn_prototypes(const Matrix& training_matrix);
	void learn_hash_prototypes(const Matrix& training_matrix);
	float*** create_lookup_table(const Matrix& target_matrix);
	void quantize_lookup_table(float*** lookup_f, int n);
	void fit(const Matrix& training_matrix, const Matrix& target_matrix);
};
