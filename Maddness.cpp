#include "Maddness.h"
#include <iostream>
#include <immintrin.h>
#include <algorithm>
#include <numeric>

Maddness::Maddness(int c, int k) : c(c), k(k) {
	prototypes.reserve(c);
	hash_prototypes.reserve(c);
}

void Maddness::learn_prototypes(const Matrix& training_matrix) {
	std::vector<std::vector<Vector>> vecs(c);
	int subspace_size = training_matrix.n / c;
	int subscpace_r = training_matrix.n % c;
	int last = 0;
	for (int i = 0; i < c; i++, subscpace_r--) {
		int size = subscpace_r > 0 ? subspace_size + 1 : subspace_size;
		for (int j = 0; j < training_matrix.m; j++) {
			vecs[i].push_back(
				Vector(training_matrix, j, last, last + size)
			);
		}
		last += size;
	}

	for (std::vector<Vector>& subspace : vecs) {
		prototypes.push_back(k_means(subspace, k));
	}
}

void Maddness::learn_hash_prototypes(const Matrix& training_matrix) {
	std::vector<std::vector<Vector>> vecs(c);
	int subspace_size = training_matrix.n / c;
	int subscpace_r = training_matrix.n % c;
	int last = 0;
	for (int i = 0; i < c; i++, subscpace_r--) {
		indices.push_back(last);
		int size = subscpace_r > 0 ? subspace_size + 1 : subspace_size;
		for (int j = 0; j < training_matrix.m; j++) {
			vecs[i].push_back(
				Vector(training_matrix, j, last, last + size)
			);
		}
		last += size;
	}
	indices.push_back(last);

	for (std::vector<Vector>& subspace : vecs) {
		std::vector<std::vector<Vector>> bucket = { subspace };
		hash_prototypes.push_back({});
		bucket = hash_prototypes.back().learn_parameters(bucket);
		std::vector<Vector> subspace_averages(bucket.size());
		std::transform(bucket.begin(), bucket.end(), subspace_averages.begin(), 
			[](std::vector<Vector>& vecs) -> Vector {
			return std::reduce(vecs.begin(), vecs.end(), Vector(vecs[0].n)) * (1.0 / vecs.size());
		});

		averages.push_back(subspace_averages);
	}
}

float*** Maddness::create_lookup_table(const Matrix& target_matrix) {
	n = target_matrix.n;
	float*** lookup_f = new float**[target_matrix.n];
	for (int i = 0; i < target_matrix.n; i++) {
		lookup_f[i] = new float*[c];
		for (int j = 0; j < c; j++) {
			lookup_f[i][j] = new float[k];
		}
	}

	Matrix target_transposed = target_matrix.transpose();
	for (int i = 0; i < target_matrix.n; i++) {
		for (int j = 0; j < averages.size(); j++) {
			std::vector<Vector> k_averages = averages[j];
			int start = indices[j];
			int end = indices[j + 1];
			Vector target_vec(target_transposed, i, start, end);
			for (int k = 0; k < k_averages.size(); k++) {
				Vector average = k_averages[k];
				float dot = target_vec.dot(average);
				lookup_f[i][j][k] = dot;
			}
		}
	}
	return lookup_f;
}

void Maddness::quantize_lookup_table(float*** lookup_f, int n) {
	float min_val = std::numeric_limits<float>::max();
	float max_val = std::numeric_limits<float>::min();

	for (int nn = 0; nn < n; nn++) {
		for (int cc = 0; cc < c; cc++) {
			for (int kk = 0; kk < k; kk++) {
				min_val = std::min(min_val, lookup_f[nn][cc][kk]);
				max_val = std::max(max_val, lookup_f[nn][cc][kk]);
			}
		}
	}
	
	offset = min_val;
	inverse_scaling = 255.0 / (max_val - min_val);
	lookup_table = new uint8_t**[n];

	for (int nn = 0; nn < n; nn++) {
		lookup_table[nn] = new uint8_t*[c];
		for (int cc = 0; cc < c; cc++) {
			lookup_table[nn][cc] = new uint8_t[2 * k];
			for (int kk = 0; kk < k; kk++) {
				lookup_table[nn][cc][kk] = (lookup_f[nn][cc][kk] - offset) * inverse_scaling;
			}
			delete[] lookup_f[nn][cc];
		}
		delete[] lookup_f[nn];
	}
	delete[] lookup_f;
}

void Maddness::fit(const Matrix& training_matrix, const Matrix& target_matrix) {
	learn_hash_prototypes(training_matrix);
	float*** lookup_f = create_lookup_table(target_matrix);
	quantize_lookup_table(lookup_f, target_matrix.n);
}

Maddness::~Maddness() {
	for (int nn = 0; nn < n; nn++) {
		for (int cc = 0; cc < c; cc++) {
			delete[] lookup_table[nn][cc];
		}
		delete[] lookup_table[nn];
	}
	delete[] lookup_table;
}

Matrix Maddness::predict(const Matrix& matrix) {
	constexpr uint8_t simd_size = 32;
	Matrix mat(matrix.m, n);
	uint8_t buf[simd_size];
	uint8_t** columns_buf = new uint8_t*[HashTree::TREE_DEPTH];

	for (int i = 0; i <  HashTree::TREE_DEPTH; i++)
		columns_buf[i] = new uint8_t[matrix.m];

	for (int i = 0; i < hash_prototypes.size(); i++) {
		HashTree hash_tree = hash_prototypes[i];
		int start = indices[i];
		int end = indices[i + 1];
		hash_tree.quantize(matrix, columns_buf, start);
		for (int row = 0; row < mat.m; row += simd_size) {
			__m256i indexes = hash_tree.hash_columns(columns_buf, row);
			for (int col = 0; col < mat.n; col++) {
				__m256i products = _mm256_shuffle_epi8(*(__m256i*) lookup_table[col][i], indexes);
				_mm256_store_si256((__m256i*) buf, products);
				for (int b = 0; b < simd_size; b++)
					mat.values[(row + b) * mat.n + col] += buf[b];
			}
		}
	}

	for (int row = 0; row < mat.m; row++) {
		for (int col = 0; col < mat.n; col++) {
			mat.values[row * mat.n + col] = mat.values[row * mat.n + col] / inverse_scaling + offset;
		}
	}

	for (int i = 0; i < HashTree::TREE_DEPTH; i++)
		delete[] columns_buf[i];
	delete[] columns_buf;
	return mat;
}
