#include "Utils.h"

#include <random>
#include <iostream>
#include <assert.h>]
#include <vector>
#include <chrono>
#include <algorithm>
#include <numeric>


std::vector<int> generate_random_numbers(int start, int end, int n) {
	assert(n < end - start);
	std::vector<int> numbers;

	for (int i = start; i < end; i++)       // add 0-99 to the vector
		numbers.push_back(i);

	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::shuffle(numbers.begin(), numbers.end(), std::default_random_engine(seed));

	std::vector<int> newVec(numbers.begin(), numbers.begin() + n);
	return newVec;
}


std::vector<Vector> k_means(const std::vector<Vector>& vectors, int k) {
	int n = vectors.size();
	int elems = vectors[0].n;
	std::vector<int> random_positions = generate_random_numbers(0, n, k);
	std::vector<Vector> centroids;
	centroids.resize(random_positions.size());

	std::transform(random_positions.begin(), random_positions.end(),
		centroids.begin(), [&vectors](int pos) -> Vector { return vectors[pos]; });

	while (true) {
		std::vector<std::vector<Vector>> clusters(k);

		for (const Vector& vec : vectors) {
			std::vector<float> distances(k);
			std::transform(centroids.begin(), centroids.end(), distances.begin(),
				[&vec](const Vector& v) -> float { return vec.distance(v);  });
			auto arg_min = std::min_element(distances.begin(), distances.end());
			int min_index = arg_min - distances.begin();
			clusters[min_index].push_back(vec);
		}

		std::vector<Vector> new_centroids(k);
		std::transform(clusters.begin(), clusters.end(), new_centroids.begin(),
			[elems](std::vector<Vector> vecs) -> Vector {
			auto const count = static_cast<float>(vecs.size());
			return std::reduce(vecs.begin(), vecs.end(), Vector(elems), std::plus<>()) * (1 / count);
		});

		bool equals = std::transform_reduce(centroids.begin(), centroids.end(),
			new_centroids.begin(), true, std::multiplies<>(), std::equal_to<>());

		centroids = new_centroids;

		if (equals)
			break;
	}

	return centroids;
}
