#pragma once

struct Matrix {
	float* values;
	int m, n; // m - liczba wierszy, n - liczba klolumn

	static const int PRINT_LIMIT = 5;

	Matrix(int m, int n);
	Matrix(const Matrix& other);
	~Matrix();

	Matrix& operator=(const Matrix& other);

	Matrix transpose() const;
	Matrix mul(const Matrix& other);
	void fill(float val);
	void fill_random(float min_val=0.0, float max_val=1.0);
	void print() const;
};

struct Vector {
	float* values;
	int n;

	Vector() : n(1), values(new float[1]) {};
	Vector(int n);
	Vector(const Matrix& mat, int row, int start, int end);
	Vector(const Vector& other);
	~Vector() { delete[] values; };
	
	Vector& operator=(const Vector& other);
	Vector operator+(const Vector& other) const;
	Vector operator*(float other) const;
	bool operator==(const Vector& other) const;
	//void operator+=(const Vector& other);

	float dot(const Vector& other) const;
	float distance(const Vector& other) const;
	void print() const;
	void fill(float val);

};

