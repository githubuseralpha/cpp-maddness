#pragma once
#include <cmath>
#include <cinttypes>
#include "Matrix.h"
#include "Utils.h"
#include <array>
#include <immintrin.h>


struct HashTree {
	static constexpr int TREE_DEPTH = 4;

	uint8_t indices[TREE_DEPTH];
	uint8_t thresholds[ipow(2, TREE_DEPTH) - 1];
	float offset, inverse_scaling;

	void quantize(const Matrix& matrix, uint8_t** columns, int col);
	__m256i hash_columns(uint8_t** columns, int row);

	std::vector<std::vector<Vector>> learn_parameters(std::vector<std::vector<Vector>>& buckets);


private:
	static auto get_params_for_level(std::vector<std::vector<Vector>>& buckets, int vector_size);
	static std::vector<int> select_indices(int range);
	static std::pair<float, float> split_threshold(int j, std::vector<Vector>& bucket);
	static auto apply_split(float threshold, int j, const std::vector<Vector>& bucket);
	static std::vector<float> cumulative_sse(std::vector<Vector> bucket, bool reverse);
}; 
