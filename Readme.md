## Maddness Matrix Multiplication

This repository is my implementation of this paper: https://arxiv.org/pdf/2106.10860.pdf. It allows quick calculation of the approximate result of matrix multiplication.

TO DO:
* Refactor hashing function (it may be faulty)
* Replace multiplication operations with shifts during quantization of matrix (and in reverse proccess)
* replace classical addition by averaging during summation of dot products
